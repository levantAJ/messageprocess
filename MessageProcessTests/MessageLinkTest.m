//
//  MessageLinkTest.m
//  MessageProcess
//
//  Created by AJ on 6/6/15.
//  Copyright (c) 2015 AJ. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <XCTest/XCTest.h>
#import "MessageLink.h"

@interface MessageLink (Test)

- (void)titleWithCompletion:(void (^)(NSError *))completion;

@end

@interface MessageLinkTest : XCTestCase

@property (nonatomic, strong) MessageLink *messageLinkToTest;

@end

@implementation MessageLinkTest

- (void)setUp
{
  [super setUp];
  // Put setup code here. This method is called before the invocation of each test method in the class.
  self.messageLinkToTest = [[MessageLink alloc] initWithURL:[NSURL URLWithString:[@"https://www.atlassian.com" stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]]];
}

- (void)tearDown
{
  // Put teardown code here. This method is called after the invocation of each test method in the class.
  [super tearDown];
}

#pragma mark - Test Method titleWithCompletion

- (void)testAtlassianTitleWithCompletion
{
  // Using XCTestExpectation to test asynchronous task
  XCTestExpectation *expectation = [self expectationWithDescription:@"Testing method testAtlassianTitleWithCompletion"];
  [self.messageLinkToTest titleWithCompletion:^(NSError *error) {
    if (error) {
      NSLog(@"Error: %@", error);
    } else {
      NSLog(@"Title: %@", self.messageLinkToTest.title);
      XCTAssertTrue([self.messageLinkToTest.title isEqualToString:@"Software Development and Collaboration Tools | Atlassian"], "The title is wrong!");
    }
    [expectation fulfill];
  }];
  
  [self waitForExpectationsWithTimeout:5.0
                               handler:^(NSError *error) {
                                 if (error) {
                                   XCTFail(@"Expectation Failed with error: %@", error);
                                 }
                               }];
}

- (void)testGoogleTitleWithCompletion
{
  // Using XCTestExpectation to test asynchronous task
  XCTestExpectation *expectation = [self expectationWithDescription:@"Testing method testGoogleTitleWithCompletion"];
  
  self.messageLinkToTest.URL = [NSURL URLWithString:[@"https://www.google.com" stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
  [self.messageLinkToTest titleWithCompletion:^(NSError *error) {
    if (error) {
      NSLog(@"Error: %@", error);
    } else {
      NSLog(@"Title: %@", self.messageLinkToTest.title);
      XCTAssertTrue([self.messageLinkToTest.title isEqualToString:@"Google"], "The title is wrong!");
    }
    [expectation fulfill];
  }];
  
  [self waitForExpectationsWithTimeout:5.0
                               handler:^(NSError *error) {
                                 if (error) {
                                   XCTFail(@"Expectation Failed with error: %@", error);
                                 }
                               }];
}

- (void)testMessTitleWithCompletion1
{
  // Using XCTestExpectation to test asynchronous task
  XCTestExpectation *expectation = [self expectationWithDescription:@"Testing method testMessTitleWithCompletion1"];
  
  self.messageLinkToTest.URL = [NSURL URLWithString:[@"https://qwertyuiop" stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
  [self.messageLinkToTest titleWithCompletion:^(NSError *error) {
    if (error) {
      NSLog(@"Error: %@", error);
    } else {
      NSLog(@"Title: %@", self.messageLinkToTest.title);
      XCTAssertTrue([self.messageLinkToTest.title isEqualToString:@""], "The title is wrong!");
    }
    [expectation fulfill];
  }];
  
  [self waitForExpectationsWithTimeout:5.0
                               handler:^(NSError *error) {
                                 if (error) {
                                   XCTFail(@"Expectation Failed with error: %@", error);
                                 }
                               }];
}

- (void)testMessTitleWithCompletion2
{
  // Using XCTestExpectation to test asynchronous task
  XCTestExpectation *expectation = [self expectationWithDescription:@"Testing method testMessTitleWithCompletion2"];
  
  self.messageLinkToTest.URL = [NSURL URLWithString:[@"fpt://hello.com" stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
  [self.messageLinkToTest titleWithCompletion:^(NSError *error) {
    if (error) {
      NSLog(@"Error: %@", error);
    } else {
      NSLog(@"Title: %@", self.messageLinkToTest.title);
      XCTAssertTrue([self.messageLinkToTest.title isEqualToString:@""], "The title is wrong!");
    }
    [expectation fulfill];
  }];
  
  [self waitForExpectationsWithTimeout:5.0
                               handler:^(NSError *error) {
                                 if (error) {
                                   XCTFail(@"Expectation Failed with error: %@", error);
                                 }
                               }];
}

@end
