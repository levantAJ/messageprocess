//
//  MessageTest.m
//  MessageProcess
//
//  Created by AJ on 6/5/15.
//  Copyright (c) 2015 AJ. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <XCTest/XCTest.h>
#import "Message.h"
#import "NSDictionary+Extension.h"

@interface Message (Test)

- (void)componentsWithCompletion:(void (^)(NSString *components))completion;
- (NSString *)preprocessText:(NSString *)text;
- (NSArray *)linksDetector:(NSString *)text;
- (NSArray *)emoticonsDetector:(NSString *)text;
- (NSArray *)mentionsDetector:(NSString *)text;
- (NSString *)toString:(NSDictionary *)dictionary;

@end

@interface MessageTest : XCTestCase

@property (nonatomic, strong) Message *messageToTest;

@end

@implementation MessageTest

- (void)setUp
{
  [super setUp];
  // Put setup code here. This method is called before the invocation of each test method in the class.
  self.messageToTest = [[Message alloc] init];
}

- (void)tearDown
{
  // Put teardown code here. This method is called after the invocation of each test method in the class.
  [super tearDown];
}

#pragma mark - Test Method componentsWithCompletion

// Test case: ""
- (void)testComponentsWithCompletion1
{
  // Using XCTestExpectation to test asynchronous task
  XCTestExpectation *expectation = [self expectationWithDescription:@"Testing method testComponentsWithCompletion1"];
  
  self.messageToTest.text = @"";
  [self.messageToTest componentsWithCompletion:^(NSString *components) {
    XCTAssertNil(components, "The components must be nil");
    [expectation fulfill];
  }];
  
  [self waitForExpectationsWithTimeout:5.0
                               handler:^(NSError *error) {
                                 if (error) {
                                   XCTFail(@"Expectation Failed with error: %@", error);
                                 }
                               }];
}

// Test case: "sirlevantai@gmail.com"
- (void)testComponentsWithCompletion2
{
  // Using XCTestExpectation to test asynchronous task
  XCTestExpectation *expectation = [self expectationWithDescription:@"Testing method testComponentsWithCompletion2"];
  
  self.messageToTest.text = @"sirlevantai@gmail.com";
  [self.messageToTest componentsWithCompletion:^(NSString *components) {
    XCTAssertNil(components, "The components must be nil");
    [expectation fulfill];
  }];
  
  
  [self waitForExpectationsWithTimeout:5.0
                               handler:^(NSError *error) {
                                 if (error) {
                                   XCTFail(@"Expectation Failed with error: %@", error);
                                 }
                               }];
}

// Test case: "sirlevantai@gmail.com @bob (success) http://google.com"
- (void)testComponentsWithCompletion3
{
  // Using XCTestExpectation to test asynchronous task
  XCTestExpectation *expectation = [self expectationWithDescription:@"Testing method testComponentsWithCompletion3"];
  
  self.messageToTest.text = @"sirlevantai@gmail.com @bob (success) http://google.com";
  [self.messageToTest componentsWithCompletion:^(NSString *components) {
    NSDictionary *json = @{@"mentions" : @[@"bob"], @"emoticons" : @[@"success"], @"links" : @[@{@"url" : @"http://google.com", @"title" : @"Google"}]};
    NSLog(@"\n%@\n%@", components, [json toString]);
    XCTAssertTrue([components isEqualToString:[json toString]], "The result is wrong!");
    [expectation fulfill];
  }];
  
  
  [self waitForExpectationsWithTimeout:5.0
                               handler:^(NSError *error) {
                                 if (error) {
                                   XCTFail(@"Expectation Failed with error: %@", error);
                                 }
                               }];
}

// Test case: "Olympics are starting soon, http://nbcolympics.com/"
- (void)testComponentsWithCompletion4
{
  // Using XCTestExpectation to test asynchronous task
  XCTestExpectation *expectation = [self expectationWithDescription:@"Testing method testComponentsWithCompletion4"];
  
  self.messageToTest.text = @"Olympics are starting soon, http://nbcolympics.com/";
  [self.messageToTest componentsWithCompletion:^(NSString *components) {
    NSDictionary *json = @{@"links" : @[@{@"url" : @"http://nbcolympics.com/", @"title" : @"NBC Olympics | Home of the 2016 Olympic Games in Rio"}]};
    NSLog(@"\n%@\n%@", components, [json toString]);
    XCTAssertTrue([components isEqualToString:[json toString]], "The result is wrong!");
    [expectation fulfill];
  }];
  
  
  [self waitForExpectationsWithTimeout:6.0
                               handler:^(NSError *error) {
                                 if (error) {
                                   XCTFail(@"Expectation Failed with error: %@", error);
                                 }
                               }];
}

// Test case: "@bob @john (success) such a cool feature https://twitter.com/jdorfman/status/430511497475670016"
- (void)testComponentsWithCompletion5
{
  // Using XCTestExpectation to test asynchronous task
  XCTestExpectation *expectation = [self expectationWithDescription:@"Testing method testComponentsWithCompletion5"];
  
  self.messageToTest.text = @"@bob @john (success) such a cool feature https://twitter.com/jdorfman/status/430511497475670016";
  [self.messageToTest componentsWithCompletion:^(NSString *components) {
    NSDictionary *json = @{@"mentions" : @[@"bob", @"john"], @"emoticons" : @[@"success"], @"links" : @[@{@"url" : @"https://twitter.com/jdorfman/status/430511497475670016", @"title" : @"Justin Dorfman on Twitter: \"nice @littlebigdetail from @HipChat (shows hex colors when pasted in chat). http://t.co/7cI6Gjy5pq\""}]};
    NSLog(@"\n%@\n%@", components, [json toString]);
    XCTAssertTrue([components isEqualToString:[json toString]], "The result is wrong!");
    [expectation fulfill];
  }];
  
  [self waitForExpectationsWithTimeout:5.0
                               handler:^(NSError *error) {
                                 if (error) {
                                   XCTFail(@"Expectation Failed with error: %@", error);
                                 }
                               }];
}

#pragma mark - Test Method preprocessText

- (void)testPreprocessText
{
  NSString *text = @"sirlevantai@gmail.com";
  text = [self.messageToTest preprocessText:text];
  XCTAssertTrue([text isEqualToString:@""], "The result is wrong");
  
  text = @"hello abc@xyzad.qwer world";
  text = [self.messageToTest preprocessText:text];
  XCTAssertTrue([text isEqualToString:@"hello  world"], "The result is wrong");
}


#pragma mark - Test Method testLinksDetector

- (void)testLinksDetector
{
  NSString *text = @"google.com";
  NSArray *matches = [self.messageToTest linksDetector:text];
  XCTAssertNotNil(matches, "The result can not be nil");
  
  NSURL *URL = (NSURL *)matches.firstObject;
  NSLog(@"%@", URL.relativeString);
  XCTAssertTrue([URL.relativeString isEqualToString:@"http://google.com"], "The result is wrong!");
  
  text = @"hello world";
  matches = [self.messageToTest linksDetector:text];
  XCTAssertEqual(matches.count, 0, "The result must be zero");
  
  text = @"nice google.com, good job men!";
  matches = [self.messageToTest linksDetector:text];
  XCTAssertNotNil(matches, "The result can not be nil");
  
  // This is bad URL
  text = @"hello world http://qwertyuiop";
  matches = [self.messageToTest linksDetector:text];
  XCTAssertEqual(matches.count, 0, "The result must be have only one link");
  
  // This is bad URL
  text = @"hello world http://asdfghjkl, nice to meet you!";
  matches = [self.messageToTest linksDetector:text];
  XCTAssertEqual(matches.count, 0, "The result must be have only one link");
  
  // This is bad URL
  text = @"hello world http://asdfghjkl://qwertyuiopdfds, nice to meet you!";
  matches = [self.messageToTest linksDetector:text];
  XCTAssertEqual(matches.count, 0, "The result must be have only one link");
  
  text = @"hello google.com world http://qwertyuiop://qwertyuiop, nice to meet you! ftp://bob.com";
  matches = [self.messageToTest linksDetector:text];
  XCTAssertEqual(matches.count, 2, "The result must be have three links");
  
  text = @"hello abc@gmail.com world";
  text = [self.messageToTest preprocessText:text];
  matches = [self.messageToTest linksDetector:text];
  XCTAssertEqual(matches.count, 0, "The result must be have zero links");
  
  text = @"hello ftp://download.com world";
  text = [self.messageToTest preprocessText:text];
  XCTAssertEqual(matches.count, 0, "The result must be have one links");
  
}

#pragma mark - Test Method emoticonsDetector

- (void)testEmoticonsDetector
{
  // Test case 1
  NSString *text = @"(david)";
  NSArray *matches = [self.messageToTest emoticonsDetector:text];
  XCTAssertNotNil(matches, "The result can not be nil");
  
  // Test case 2
  text = @"hello (david) nice to meet you!";
  matches = [self.messageToTest emoticonsDetector:text];
  XCTAssertNotNil(matches, "The result can not be nil");
  
  NSString *emoticon = matches.firstObject;
  XCTAssertTrue([emoticon isEqualToString:@"david"], "The result is wrong!");
  
  // Test case 3
  text = @"hello ((david)) nice to meet you!";
  matches = [self.messageToTest emoticonsDetector:text];
  XCTAssertNotNil(matches, "The result can not be nil");
  
  emoticon = matches.firstObject;
  XCTAssertTrue([emoticon isEqualToString:@"david"], "The result is wrong!");
  
  // Test case 4
  text = @"hello ((david)) nice (pokerface) to meet you!";
  matches = [self.messageToTest emoticonsDetector:text];
  XCTAssertEqual(matches.count, 2, "Size of result must be two");
  
  // Test case 5
  text = @"hello ((david beckham)) nice (poker face) to meet you!";
  matches = [self.messageToTest emoticonsDetector:text];
  XCTAssertEqual(matches.count, 0, "Size of result must be zero");
  
  // Test case 6
  text = @"(123)";
  matches = [self.messageToTest emoticonsDetector:text];
  XCTAssertEqual(matches.count, 1, "Size of result must be one");
  
  emoticon = matches.firstObject;
  XCTAssertTrue([emoticon isEqualToString:@"123"], "The result is wrong!");
  
  // Test case 7
  text = @"(123abc";
  matches = [self.messageToTest emoticonsDetector:text];
  XCTAssertEqual(matches.count, 0, "Size of result must be zero");
  
  // Test case 8
  text = @"123abc)";
  matches = [self.messageToTest emoticonsDetector:text];
  XCTAssertEqual(matches.count, 0, "Size of result must be zero");
  
  // Test case 9
  text = @"(Mountain)";
  matches = [self.messageToTest emoticonsDetector:text];
  XCTAssertEqual(matches.count, 1, "Size of result must be one");
  
  emoticon = matches.firstObject;
  XCTAssertTrue([emoticon isEqualToString:@"Mountain"], "The result is wrong!");
  
  // Test case 10
  text = @"(sucess(bad(guy)))";
  matches = [self.messageToTest emoticonsDetector:text];
  XCTAssertEqual(matches.count, 1, "Size of result must be zero");
}

#pragma mark - Test Method mentiosDetector
- (void)testMentionsDetector
{
  // Test case 1
  NSString *text = @"@david0";
  NSArray *matches = [self.messageToTest mentionsDetector:text];
  XCTAssertNotNil(matches, "The result can not be nil");
  XCTAssertEqual(matches.count, 1, "Size of matches must be one");
  
  NSString *mention = matches.firstObject;
  XCTAssertNotNil(mention, "The mention can not be nil");
  XCTAssertTrue([mention isEqualToString:@"david0"], "The result is wrong!");
  
  // Test case 2
  text = @"hello @david) right here?";
  matches = [self.messageToTest mentionsDetector:text];
  XCTAssertNotNil(matches, "The result can not be nil");
  XCTAssertEqual(matches.count, 1, "Size of matches must be one");
  
  mention = matches.firstObject;
  XCTAssertNotNil(mention, "The mention can not be nil");
  XCTAssertTrue([mention isEqualToString:@"david"], "The result is wrong!");
  
  // Test case 3
  text = @"hello @david0 right here?";
  matches = [self.messageToTest mentionsDetector:text];
  XCTAssertNotNil(matches, "The result can not be nil");
  XCTAssertEqual(matches.count, 1, "Size of matches must be one");
  
  mention = matches.firstObject;
  XCTAssertNotNil(mention, "The mention can not be nil");
  XCTAssertTrue([mention isEqualToString:@"david0"], "The result is wrong!");
  
  // Test case 4
  text = @"@PeteCurley @david right here?";
  matches = [self.messageToTest mentionsDetector:text];
  XCTAssertNotNil(matches, "The result can not be nil");
  XCTAssertEqual(matches.count, 2, "Size of matches must be one");
  
  mention = matches.firstObject;
  XCTAssertNotNil(mention, "The mention can not be nil");
  XCTAssertTrue([mention isEqualToString:@"PeteCurley"], "The result is wrong!");
  
  mention = matches.lastObject;
  XCTAssertNotNil(mention, "The mention can not be nil");
  XCTAssertTrue([mention isEqualToString:@"david"], "The result is wrong!");
  
  // Test case 5
  text = @"@";
  matches = [self.messageToTest mentionsDetector:text];
  XCTAssertNotNil(mention, "The mention can not be nil");
  XCTAssertEqual(matches.count, 0, "Size of matches must be zero");
  
  // Test case 6
  text = @" @ ";
  matches = [self.messageToTest mentionsDetector:text];
  XCTAssertNotNil(mention, "The mention can not be nil");
  XCTAssertEqual(matches.count, 0, "Size of matches must be zero");
  
  // Test case 7
  text = @" @@ ";
  matches = [self.messageToTest mentionsDetector:text];
  XCTAssertNotNil(mention, "The mention can not be nil");
  XCTAssertEqual(matches.count, 0, "Size of matches must be zero");
  
  // Test case 8
  text = @"david@david";
  matches = [self.messageToTest mentionsDetector:text];
  XCTAssertNotNil(mention, "The mention can not be nil");
  XCTAssertEqual(matches.count, 0, "Size of matches must be one");
  
  // Test case 9
  text = @"david@david.com";
  text = [self.messageToTest preprocessText:text];
  matches = [self.messageToTest mentionsDetector:text];
  XCTAssertNotNil(mention, "The mention can not be nil");
  XCTAssertEqual(matches.count, 0, "Size of matches must be zero");
  
  // Test case 10
  text = @"david%@david.com";
  text = [self.messageToTest preprocessText:text];
  matches = [self.messageToTest mentionsDetector:text];
  XCTAssertNotNil(mention, "The mention can not be nil");
  XCTAssertEqual(matches.count, 0, "Size of matches must be zero");
  
  // Test case 11
  text = @"(@man)";
  matches = [self.messageToTest mentionsDetector:text];
  XCTAssertNotNil(mention, "The mention can not be nil");
  XCTAssertEqual(matches.count, 1, "Size of matches must be one");
  
  // Test case 12
  text = @"@man@girl@don";
  matches = [self.messageToTest mentionsDetector:text];
  XCTAssertNotNil(mention, "The mention can not be nil");
  XCTAssertEqual(matches.count, 3, "Size of matches must be three");
  
  mention = [matches objectAtIndex:0];
  XCTAssertTrue([mention isEqualToString:@"man"], "The mentions at 0 is wrong");
  
  mention = [matches objectAtIndex:1];
  XCTAssertTrue([mention isEqualToString:@"girl"], "The mentions at 1 is wrong");
  
  mention = [matches objectAtIndex:2];
  XCTAssertTrue([mention isEqualToString:@"don"], "The mentions at 2 is wrong");
}


@end
