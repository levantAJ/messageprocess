//
//  NSURL+Extension.m
//  MessageProcess
//
//  Created by AJ on 6/8/15.
//  Copyright (c) 2015 AJ. All rights reserved.
//

#import "NSURL+Extension.h"

@implementation NSURL (Extension)


+ (NSURL *)HTTPURLFromString:(NSString *)string
{
  if ([string hasPrefix:@"http://"] || [string hasPrefix:@"https://"] || [string hasPrefix:@"ftp://"]) {
    return [NSURL URLWithString:[string stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
  }
  string = [NSString stringWithFormat:@"http://%@", string];
  
  return [NSURL URLWithString:[string stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
}

@end
