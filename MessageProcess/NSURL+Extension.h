//
//  NSURL+Extension.h
//  MessageProcess
//
//  Created by AJ on 6/8/15.
//  Copyright (c) 2015 AJ. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface NSURL (Extension)

/**
 Create NSURL, if it has no protocol: http://, https://, ftp://, then we add these protocol into the link
 @param string A string url which to check
 @return A URL has protocol
 */
+ (NSURL *)HTTPURLFromString:(NSString *)string;

@end
