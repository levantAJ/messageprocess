//
//  NSDictionary+Extension.m
//  MessageProcess
//
//  Created by AJ on 6/6/15.
//  Copyright (c) 2015 AJ. All rights reserved.
//

#import "NSDictionary+Extension.h"

@implementation NSDictionary (Extension)

- (NSString *)toString
{
  if (self && self.count > 0) {
    NSError *error;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:self
                                                       options:NSJSONWritingPrettyPrinted // Pass 0 if you don't care about the readability of the generated string
                                                         error:&error];
    
    if (jsonData) {
      NSString *jsonString = [[NSString alloc] initWithData:jsonData
                                                   encoding:NSUTF8StringEncoding];
      return jsonString;
    }
    
  }
  return nil;
}

@end
