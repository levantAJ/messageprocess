//
//  NSString+Extension.m
//  MessageProcess
//
//  Created by AJ on 6/4/15.
//  Copyright (c) 2015 AJ. All rights reserved.
//

#import "NSString+Extension.h"

@implementation NSString (Extension)

- (BOOL)isNotEmpty
{
  return [self length] != 0;
}

- (BOOL)isNotBlank
{
  if ([self isNotEmpty]) {
    NSCharacterSet *nonWhitespaceSet = [[NSCharacterSet whitespaceAndNewlineCharacterSet] invertedSet];
    NSRange range = [self rangeOfCharacterFromSet:nonWhitespaceSet];
    return range.location != NSNotFound;
  }
  
  return NO;
}

- (NSArray *)detectByPattern:(NSString *)pattern
                captureIndex:(NSInteger)captureIndex
{
  if (self) {
    NSError *error = nil;
    
    NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:pattern
                                                                           options:NSRegularExpressionCaseInsensitive
                                                                             error:&error];
    NSArray *matches = [regex matchesInString:self
                                      options:0
                                        range:NSMakeRange(0, [self length])];
    
    NSMutableArray *captures = [[NSMutableArray alloc] initWithCapacity:matches.count];
    for (NSTextCheckingResult* match in matches) {
      // Get string matches by the captureIndex
      NSString* capture = nil;
      if (captureIndex==0) {
        // Get whole matched string
        capture = [self substringWithRange:[match range]];
      }else{
        // Get the string by captureIndex
        capture = [self substringWithRange:[match rangeAtIndex:1]];
      }
      [captures addObject:capture];
    }
    return captures;
  }
  return nil;
}

@end
