//
//  ViewController.m
//  MessageProcess
//
//  Created by AJ on 6/4/15.
//  Copyright (c) 2015 AJ. All rights reserved.
//

#import "MessageViewController.h"
#import "ArrayDataSource.h"
#import "MessageCell.h"
#import "Message.h"

#import "NSString+Extension.h"
#import "UIView+Extension.h"

#import <Reachability.h>

static NSString *const MessageCellIdentifier = @"MessageCell";
static CGFloat const FooterViewBottomConstraintDefault = 0;
static CGFloat const FooterViewHeightConstraintDefault = 48;

@interface MessageViewController () <UITableViewDelegate, UITextViewDelegate, MessageDelegate>

@property (nonatomic, strong) ArrayDataSource *messagesArrayDataSource;

@end

@implementation MessageViewController

- (void)viewDidLoad
{
  [super viewDidLoad];
  // Do any additional setup after loading the view, typically from a nib.
  self.title = NSLocalizedString(@"Message", @"Title of chatting screen");
  [self commonSetup];
  
  // Manage the keyboard's events
  [self registerForKeyboardNotifications];
  
  // Check the network
  // We can use NSNotitficaiton to notify whenever the network has changed
  [self checkNetworkConnections];
}

- (void)didReceiveMemoryWarning
{
  [super didReceiveMemoryWarning];
  // Dispose of any resources that can be recreated.
}

#pragma mark - Network Connections

- (void)checkNetworkConnections
{
  dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
    // Allocate a reachability object
    Reachability* reach = [Reachability reachabilityWithHostname:@"www.google.com"];
    
    reach.unreachableBlock = ^(Reachability *reach) {
      NSLog(@"UNREACHABLE!");
      dispatch_async(dispatch_get_main_queue(), ^{
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Connections Error", @"The network connection got problem (offline)")
                                                        message:NSLocalizedString(@"You are not conneted to the Internet", @"You are not connected to the Internet")
                                                       delegate:self
                                              cancelButtonTitle:NSLocalizedString(@"OK", @"Title of alert's cancel button") otherButtonTitles:nil, nil];
        [alert show];
      });
    };
    
    // Start the notifier, which will cause the reachability object to retain itself!
    [reach startNotifier];
  });
}

#pragma mark - Keyboard notifications

/**
 Register event when the keyboard change state: hide, show, ...
 */
- (void)registerForKeyboardNotifications
{
  // Register event when keyboard will active
  NSNotificationCenter *notificationCenter = [NSNotificationCenter defaultCenter];
  [notificationCenter addObserver:self
                         selector:@selector(keyboardWillShow:)
                             name:UIKeyboardWillShowNotification
                           object:nil];
  
  // Register event when keyboard will hidden
  [notificationCenter addObserver:self
                         selector:@selector(keyboardWillBeHidden:)
                             name:UIKeyboardWillHideNotification
                           object:nil];
}

/**
 When the keyboard was show, we must be re-layout of tableView and footerView
 */
- (void)keyboardWillShow:(NSNotification *)aNotification
{
  // Re-layout of messageTextView and tableView
  [UIView animateWithDuration:0.25f
                   animations:^{
                     self.footerViewBottomConstraint.constant = [self heightFromNotification:aNotification] + FooterViewBottomConstraintDefault;
                     [self.view layoutIfNeeded];
                   }
                   completion:^(BOOL finished) {
                     // Make sure the footerView not cover last cell in uitableView
                     [self scrollToLastCell];
                   }];
}

/**
 When the keyboard is hidden, we also set the layout of tableView and footerView back to default state
 */
- (void)keyboardWillBeHidden:(NSNotification *)aNotification
{
#pragma unused (aNotification)
  // Re-layout of messageTextView and tableView
  [UIView animateWithDuration:0.25f
                   animations:^{
                     self.footerViewBottomConstraint.constant = FooterViewBottomConstraintDefault;
                     [self.view layoutIfNeeded];
                   }];
}

#pragma mark - UITextViewDelegate

/**
 If the message is long, the height of footerView and height of textView would be change
 */
- (void)textViewDidChange:(UITextView *)textView
{
  if ([textView.text isNotEmpty] && [textView.text isNotBlank]) {
    // Go to background thread to calculate the rows of uitextview
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
      CGFloat rows = floorf((self.messageTextView.contentSize.height - self.messageTextView.textContainerInset.top - self.messageTextView.textContainerInset.bottom) / self.messageTextView.font.lineHeight);
      
      // Go back to main thread to update UI
      dispatch_async(dispatch_get_main_queue(), ^{
        
        // Max height of TextView is 7 rows, if the height over 7 rows, the uitextview will at scroll state
        CGFloat height = FooterViewHeightConstraintDefault + (rows - 1) * self.messageTextView.font.lineHeight;
        
        if (height < (FooterViewBottomConstraintDefault + 7 * self.messageTextView.font.lineHeight)) {
          [UIView animateWithDuration:0.25f
                           animations:^{
                             self.footerViewHeightConstraint.constant = height;
                             [self.view layoutIfNeeded];
                           }
                           completion:^(BOOL finished) {
                             self.messageTextView.contentSize = self.messageTextView.frame.size;
                             
                             // Scroll to last cell in tableView
                             [self scrollToLastCell];
                           }];
        }
        
      });
      
    });
    
  }
}

#pragma mark - User's Interactions

- (IBAction)sendButtonTapped:(id)sender
{
#pragma unused (sender)
  if ([self.messageTextView.text isNotEmpty] && [self.messageTextView.text isNotBlank]) {
    // Create new message
    Message *message = [[Message alloc] initWithText:self.messageTextView.text];
    
    // Get data in message: mentions, links, emoticons via block
//    [message componentsWithCompletion:^(NSString *components) {
//      NSLog(@"Message: %@\nComponents: %@", message.text, components);
//      if (components) {
//        [self.tableView reloadRowsAtIndexPaths:@[[self.messagesArrayDataSource indexPathByItem:message]]
//                              withRowAnimation:UITableViewRowAnimationFade];
//        [self.tableView scrollToRowAtIndexPath:[self.messagesArrayDataSource indexPathByItem:message]
//                              atScrollPosition:UITableViewScrollPositionBottom
//                                      animated:YES];
//      }
//    }];
    
    // Get data in message: mentions, links, emoticons via delegate
    message.delegate = self;
    [message getComponents];
    
    // Add new message into table's datasource
    self.messagesArrayDataSource.items = [self.messagesArrayDataSource.items arrayByAddingObject:message];
    
    // Insert this message into tableView
    NSIndexPath *indexPath = [self.messagesArrayDataSource indexPathByItem:message];
    [self.tableView beginUpdates];
    [self.tableView insertRowsAtIndexPaths:@[indexPath]
                          withRowAnimation:UITableViewRowAnimationFade];
    [self.tableView endUpdates];
    
    // Scroll to added message cell
    [self.tableView scrollToRowAtIndexPath:indexPath
                          atScrollPosition:UITableViewScrollPositionBottom
                                  animated:YES];
    
    // Clear sent message
    self.messageTextView.text = @"";
    
    // Reset height of FooterView with animated
    [UIView animateWithDuration:0.25f animations:^{
      self.footerViewHeightConstraint.constant = FooterViewHeightConstraintDefault;
      [self.view layoutIfNeeded];
    }];
    
  }
}

#pragma mark - MessageDelegate

- (void)message:(Message *)message
didGetComponents:(NSString *)components
{
  NSLog(@"Message: %@\nComponents: %@", message.text, components);
  if (components) {
    [self.tableView reloadRowsAtIndexPaths:@[[self.messagesArrayDataSource indexPathByItem:message]]
                          withRowAnimation:UITableViewRowAnimationFade];
    [self.tableView scrollToRowAtIndexPath:[self.messagesArrayDataSource indexPathByItem:message]
                          atScrollPosition:UITableViewScrollPositionBottom
                                  animated:YES];
  }
}


#pragma mark - Private Methods

/**
 Setup layout and data in viewcontroller
 */
- (void)commonSetup
{
  [self setupTableView];
  [self setupFooterView];
}

/**
 Setup the tableview which contain the conversations
 */
- (void)setupTableView
{
  // Configure cell through by TableViewCellConfigureBlock
  TableViewCellConfigureBlock configureCell = ^(MessageCell *cell, Message *message) {
    [cell configureForMessage:message];
  };
  NSArray *messages = @[];
  self.messagesArrayDataSource = [[ArrayDataSource alloc] initWithItems:messages
                                                         cellIdentifier:MessageCellIdentifier
                                                     configureCellBlock:configureCell];
  self.tableView.dataSource = self.messagesArrayDataSource;
  self.tableView.delegate = self;
  [self.tableView registerNib:[MessageCell nib]
       forCellReuseIdentifier:MessageCellIdentifier];
  
  // Remove blank cells
  self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
  self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
  
  // Set the rowHeight property on the table view to the constant UITableViewAutomaticDimension.
  self.tableView.rowHeight = UITableViewAutomaticDimension;
  // Enable row height estimation by setting the table view's estimatedRowHeight property to a nonzero value
  self.tableView.estimatedRowHeight = 44.0f;
}

/**
 Setup the view which contain the controls to edit the message
 */
- (void)setupFooterView
{
  // Setup layout of the messageTextView
  // Add top border for footerView
  [self.footerView addTopBorderWithColor:[UIColor lightGrayColor]
                                andWidth:0.5f];
  
  self.messageTextView.layer.cornerRadius = 3;
  self.messageTextView.layer.borderColor = [UIColor lightGrayColor].CGColor;
  self.messageTextView.layer.borderWidth = 0.5;
  self.messageTextView.delegate = self;
  
  self.footerViewBottomConstraint.constant = FooterViewBottomConstraintDefault;
  self.footerViewHeightConstraint.constant = FooterViewHeightConstraintDefault;
}

/**
 Get height of keyboard base on the notification
 */
- (CGFloat)heightFromNotification:(NSNotification *)aNotification
{
  NSDictionary* keyboardInfo = [aNotification userInfo];
  CGRect keyboardFrame = [[keyboardInfo valueForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue];
  return CGRectGetHeight(keyboardFrame);
}

/**
 Scroll to last cell of tableView with animation
 */
- (void)scrollToLastCell
{
  if (self.messagesArrayDataSource.items.count > 0) {
    [self.tableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:self.messagesArrayDataSource.items.count-1 inSection:0] atScrollPosition:UITableViewScrollPositionBottom animated:YES];
  }
}


@end
