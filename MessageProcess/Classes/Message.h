//
//  Message.h
//  MessageProcess
//
//  Created by AJ on 6/4/15.
//  Copyright (c) 2015 AJ. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol MessageDelegate;

@interface Message : NSObject

@property (nonatomic, strong) NSString *text;
@property (nonatomic, strong) NSString *componentsText;
@property (nonatomic, weak) id<MessageDelegate> delegate;

- (id)initWithText:(NSString *)text;

/**
 Get message data which are: metions, emoticons, links, create request to get title of links and execute them underneath a new thead
 @param completion A block object to be executed when the operations finishes successfully. This block has no return value and takes one argument: the NSStirng created from the message data
 */
- (void)componentsWithCompletion:(void (^)(NSString *components))completion;

/**
 Get message data which are: metions, emoticons, links, create request to get title of links and execute them underneath a new thead, when method finished, it will invoke the delegate:
 */
- (void)getComponents;

@end

@protocol MessageDelegate <NSObject>

@required

/**
 Method will be invoked after the method getcomponents finish
 */
- (void)message:(Message *)message
didGetComponents:(NSString *)components;

@end


