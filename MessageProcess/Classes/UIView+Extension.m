//
//  UIView+Extension.m
//  MessageProcess
//
//  Created by AJ on 6/7/15.
//  Copyright (c) 2015 AJ. All rights reserved.
//

#import "UIView+Extension.h"

@implementation UIView (Extension)

- (void)addTopBorderWithColor:(UIColor *)color
                         andWidth:(CGFloat)borderWidth
{
  CALayer *border = [CALayer layer];
  border.backgroundColor = color.CGColor;
  
  border.frame = CGRectMake(0, 0, CGRectGetWidth(self.frame), borderWidth);
  [self.layer addSublayer:border];
}


@end
