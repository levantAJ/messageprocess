//
//  NSDictionary+Extension.h
//  MessageProcess
//
//  Created by AJ on 6/6/15.
//  Copyright (c) 2015 AJ. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSDictionary (Extension)

/**
 Method to convert itseld to the json string
 @return The string with json format
 */
- (NSString *)toString;

@end
