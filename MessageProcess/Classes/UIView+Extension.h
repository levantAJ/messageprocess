//
//  UIView+Extension.h
//  MessageProcess
//
//  Created by AJ on 6/7/15.
//  Copyright (c) 2015 AJ. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIView (Extension)

/**
 Add top border for view
 @param color The color of border
 @param borderWidth The width of border
 */
- (void)addTopBorderWithColor:(UIColor *)color
                     andWidth:(CGFloat)borderWidth;

@end
