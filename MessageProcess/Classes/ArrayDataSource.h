//
//  ArrayDataSource.h
//  MessageProcess
//
//  Created by AJ on 6/4/15.
//  Copyright (c) 2015 AJ. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void(^TableViewCellConfigureBlock)(id cell, id item);

@interface ArrayDataSource : NSObject <UITableViewDataSource>

@property (nonatomic, strong) NSArray *items;

- (id)initWithItems:(NSArray *)anItems
     cellIdentifier:(NSString *)aCellIdentifier
 configureCellBlock:(TableViewCellConfigureBlock)aConfigureCellBlock;

/**
 Get object in items with an indexPath
 @param indexPath An IndexPath where the object located on
 @return A object located at indexPath on items
 */
- (instancetype)itemAtIndexPath:(NSIndexPath *)indexPath;

/**
 Get IndexPath of item in items
 @param item An object to be find in items
 @return An index path of items
 */
- (NSIndexPath *)indexPathByItem:(id)item;


@end
