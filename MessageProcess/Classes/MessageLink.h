//
//  MessageLink.h
//  MessageProcess
//
//  Created by AJ on 6/5/15.
//  Copyright (c) 2015 AJ. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MessageLink : NSObject

@property (nonatomic, strong) NSURL *URL;
@property (nonatomic, strong) NSString *title;

- (id)initWithURL:(NSURL *)URL;

/**
 Get title of crawled html by the URL
 @param completion A block object to be executed when the request get title finishes successfully. This block has no return value and takes one argument: the error object created from the response data of request.
 */
- (void)titleWithCompletion:(void (^)(NSError *error))completion;

/**
 Convert NSDictionary to string
 @return json performing for an object with "url" and "title"
 */
- (NSDictionary *)toJSON;


@end
