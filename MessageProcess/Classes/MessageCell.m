//
//  MessageCell.m
//  MessageProcess
//
//  Created by AJ on 6/4/15.
//  Copyright (c) 2015 AJ. All rights reserved.
//

#import "MessageCell.h"
#import "Message.h"

@implementation MessageCell

- (void)awakeFromNib
{
  // Disable the effects when users touch on cell, because we have override the highlight method, it replace for selectionStyle
  self.selectionStyle = UITableViewCellSelectionStyleNone;
  self.wrapperView.layer.cornerRadius = 2;
  self.componentsLabel.text = @"";
}

- (void)layoutSubviews
{
  // Update layout if needed
  [self.contentView setNeedsLayout];
  [self.contentView layoutIfNeeded];
  
  // Auto resize contentLabel
  self.contentLabel.preferredMaxLayoutWidth = CGRectGetWidth(self.contentLabel.bounds);
  
  [super layoutSubviews];
}

/**
 Override the highlight method when users have any touch on cell
 - Make shadow for contentLabel
 */
- (void)setHighlighted:(BOOL)highlighted
              animated:(BOOL)animated
{
  [super setHighlighted:highlighted animated:animated];
  if (highlighted) {
    self.contentLabel.shadowColor = [UIColor darkGrayColor];
    self.contentLabel.shadowOffset = CGSizeMake(1, 1);
  } else {
    self.contentLabel.shadowColor = nil;
  }
}

+ (UINib *)nib
{
  return [UINib nibWithNibName:[self reuseId] bundle:nil];
}

+ (NSString *)reuseId
{
  return NSStringFromClass([self class]);
}

- (CGFloat)heightForWidth:(CGFloat)width
{
  // Make sure constraints have been added to this cell, since it may have just been created from scratch
  [self setNeedsUpdateConstraints];
  [self updateConstraintsIfNeeded];
  
  self.bounds = CGRectMake(0.0f, 0.0f, width, CGRectGetHeight(self.bounds));
  
  // Do the layout pass on the cell, which will calculate the frames for all the views based on the constraints
  // (Note that the preferredMaxLayoutWidth is set on multi-line UILabels inside the -[layoutSubviews] method
  // in the UITableViewCell subclass
  [self setNeedsLayout];
  [self layoutIfNeeded];
  
  // Get the actual height required for the cell
  CGFloat height = [self.contentView systemLayoutSizeFittingSize:UILayoutFittingCompressedSize].height;
  
  // Add an extra point to the height to account for the cell separator, which is added between the bottom
  // of the cell's contentView and the bottom of the table view cell.
  height += 1;
  
  return height;
}

- (void)configureForMessage:(Message *)message
{
  self.contentLabel.text = message.text;
  if (message.componentsText) {
    self.componentsLabel.text = message.componentsText;
  } else {
    self.componentsLabel.text = @"";
  }
}


@end
