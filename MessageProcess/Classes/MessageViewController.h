//
//  ViewController.h
//  MessageProcess
//
//  Created by AJ on 6/4/15.
//  Copyright (c) 2015 AJ. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MessageViewController : UIViewController

@property (nonatomic, weak) IBOutlet UIView *footerView;
@property (nonatomic, weak) IBOutlet UITableView *tableView;
@property (nonatomic, weak) IBOutlet UITextView *messageTextView;

// NSLayoutConstraints
@property (nonatomic, weak) IBOutlet NSLayoutConstraint *footerViewBottomConstraint;
@property (nonatomic, weak) IBOutlet NSLayoutConstraint *footerViewHeightConstraint;
@property (nonatomic, weak) IBOutlet NSLayoutConstraint *messageTextViewHeightConstraint;

@end

