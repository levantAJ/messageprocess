//
//  MessageLink.m
//  MessageProcess
//
//  Created by AJ on 6/5/15.
//  Copyright (c) 2015 AJ. All rights reserved.
//

#import "MessageLink.h"
#import <HTMLReader/HTMLReader.h>

@implementation MessageLink

- (id)initWithURL:(NSURL *)URL
{
  self = [super init];
  if (self) {
    self.URL = URL;
  }
  return self;
}

- (void)titleWithCompletion:(void (^)(NSError *))completion
{
  // Get html and parsing to get page's title
  // Using NSURLSession to create request to get HTML code from the link
  NSURLSession *session = [NSURLSession sharedSession];
  [[session dataTaskWithURL:self.URL
          completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
            
            // Get content type of response
            NSString *contentType = nil;
            if ([response isKindOfClass:[NSHTTPURLResponse class]]) {
              NSDictionary *headers = [(NSHTTPURLResponse *)response allHeaderFields];
              contentType = headers[@"Content-Type"];
            }
            HTMLDocument *document = [HTMLDocument documentWithData:data
                                                  contentTypeHeader:contentType];
            self.title = [document firstNodeMatchingSelector:@"title"].textContent;
            
            // Execute the block to invoke task in the callers
            if (completion) {
              completion(error);
            }
            
          }] resume];
}

#pragma mark - Private Methods

/**
 Convert itself to the the JSON (NSDictionary) object
 */
- (NSDictionary *)toJSON
{
  return @{@"url" : [self.URL relativeString], @"title" : self.title ? self.title : @""};
}

@end
