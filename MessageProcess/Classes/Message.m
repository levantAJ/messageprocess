//
//  Message.m
//  MessageProcess
//
//  Created by AJ on 6/4/15.
//  Copyright (c) 2015 AJ. All rights reserved.
//

#import "Message.h"
#import "MessageLink.h"
#import "NSString+Extension.h"
#import "NSDictionary+Extension.h"
#import "NSURL+Extension.h"

@implementation Message

- (id)initWithText:(NSString *)text
{
  self = [super init];
  if (self) {
    self.text = text;
  }
  return self;
}

- (void)componentsWithCompletion:(void (^)(NSString *components))completion
{
  [self processWithBlock:completion];
}

- (void)getComponents
{
  // Block is nil - When tasks completed it will invoke via delegate
  [self processWithBlock:nil];
}

#pragma mark - Private Methods

/**
 Process the message to get components: links, mentions, emoticon
 @param completion A block object to be executed when the operations finishes successfully. This block has no return value and takes one argument: the NSStirng created from the message data. This block is nil, the method will invoke via delegate
 */
- (void)processWithBlock:(void (^)(NSString *components))completion
{
  dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
    
    // Preprocess message before detect comppnents
    NSString *processedText = [self preprocessText:self.text];
    
    // Create group to notify when task completed
    dispatch_group_t group = dispatch_group_create();
    
    // GET LINKS, process to get page's title from the links
    NSArray *URLs = [self linksDetector:processedText];
    NSMutableArray *links = [[NSMutableArray alloc] initWithCapacity:URLs.count];
    if (URLs) {
      for (NSURL *URL in URLs) {
        
        // Add task into group
        dispatch_group_enter(group);
        
        MessageLink *messageLink = [[MessageLink alloc] initWithURL:URL];
        [messageLink titleWithCompletion:^(NSError *error) {
          [links addObject:[messageLink toJSON]];
          
          // Leave group when task completed
          dispatch_group_leave(group);
        }];
        
      }
    }
    
    // GET EMOTICONS
    NSArray *emoticons = [self emoticonsDetector:processedText];
    
    // GET MENTIONS
    NSArray *mentions = [self mentionsDetector:processedText];
    
    // Task completed
    dispatch_group_notify(group, dispatch_get_main_queue(), ^{
      NSMutableDictionary *data = [[NSMutableDictionary alloc] init];
      
      // Add mentions to result
      if (mentions && mentions.count > 0) {
        [data setObject:mentions forKey:@"mentions"];
      }
      
      // Add emoticons to result
      if (emoticons && emoticons.count > 0) {
        [data setObject:emoticons forKey:@"emoticons"];
      }
      
      // Add links to result
      if (links && links.count > 0) {
        [data setObject:links forKey:@"links"];
      }
      self.componentsText = [data toString]; // ? [data toString] : @"";
      
      if (completion) {
        // Invoke via block
        completion(self.componentsText);
      } else {
       
        // Invoke via delegate
        id<MessageDelegate> strongDelegate = self.delegate;
        if ([strongDelegate respondsToSelector:@selector(message:didGetComponents:)]) {
          [strongDelegate message:self didGetComponents:self.componentsText];
        }
      }
      
    });
    
  });
}

/**
 Preprocess the message before detect components
 - Remove email
 @param text The string need to be detect components
 @return string The string after remove email (because email not a URL)
 */
- (NSString *)preprocessText:(NSString *)text
{
  // Removing email - Email not a URL
  // If have email that make confusing its a URL
  // Email make detect metions incorrect
  NSError *error = nil;
  NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:@"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}"
                                                                         options:NSRegularExpressionCaseInsensitive
                                                                           error:&error];
  
  return [regex stringByReplacingMatchesInString:text
                                         options:0
                                           range:NSMakeRange(0, [text length])
                                    withTemplate:@""];
}

/**
 Get all URLs in the text
 @param text The string need to be detect links
 @return list of NSURL in text
 */
- (NSArray *)linksDetector:(NSString *)text
{
  return [self linksByNSRegularExpression:text];
}

/**
 Get all URLs in the text using NSRegularExpression
 @param text The string need to be detect links
 @return list of NSURL in text
 */
- (NSArray *)linksByNSRegularExpression:(NSString *)text
{
  if (text) {
    NSArray *links = [text detectByPattern:@"(?i)\\b(?:[a-z][\\w\\-]+://(?:\\S+?(?::\\S+?)?\\@)?)?(?:(?:(?<!:/|\\.)(?:(?:[a-z0-9\\-]+\\.)+(?:(ac|ad|ae|aero|af|ag|ai|al|am|an|ao|aq|ar|arpa|as|asia|at|au|aw|ax|az|ba|bb|bd|be|bf|bg|bh|bi|biz|bj|bm|bn|bo|br|bs|bt|bv|bw|by|bz|ca|cat|cc|cd|cf|cg|ch|ci|ck|cl|cm|cn|co|com|coop|cr|cu|cv|cx|cy|cz|de|dj|dk|dm|do|dz|ec|edu|ee|eg|er|es|et|eu|fi|fj|fk|fm|fo|fr|ga|gb|gd|ge|gf|gg|gh|gi|gl|gm|gn|gov|gp|gq|gr|gs|gt|gu|gw|gy|hk|hm|hn|hr|ht|hu|id|ie|il|im|in|info|int|io|iq|ir|is|it|je|jm|jo|jobs|jp|ke|kg|kh|ki|km|kn|kp|kr|kw|ky|kz|la|lb|lc|li|lk|lr|ls|lt|lu|lv|ly|ma|mc|md|me|mg|mh|mil|mk|ml|mm|mn|mo|mobi|mp|mq|mr|ms|mt|mu|museum|mv|mw|mx|my|mz|na|name|nc|ne|net|nf|ng|ni|nl|no|np|nr|nu|nz|om|org|pa|pe|pf|pg|ph|pk|pl|pm|pn|pr|pro|ps|pt|pw|py|qa|re|ro|rs|ru|rw|sa|sb|sc|sd|se|sg|sh|si|sj|sk|sl|sm|sn|so|sr|st|su|sv|sy|sz|tc|td|tel|tf|tg|th|tj|tk|tl|tm|tn|to|tp|tr|travel|tt|tv|tw|tz|ua|ug|uk|us|uy|uz|va|vc|ve|vg|vi|vn|vu|wf|ws|xn--0zwm56d|xn--11b5bs3a9aj6g|xn--3e0b707e|xn--45brj9c|xn--80akhbyknj4f|xn--90a3ac|xn--9t4b11yi5a|xn--clchc0ea0b2g2a9gcd|xn--deba0ad|xn--fiqs8s|xn--fiqz9s|xn--fpcrj9c3d|xn--fzc2c9e2c|xn--g6w251d|xn--gecrj9c|xn--h2brj9c|xn--hgbk6aj7f53bba|xn--hlcj6aya9esc7a|xn--j6w193g|xn--jxalpdlp|xn--kgbechtv|xn--kprw13d|xn--kpry57d|xn--lgbbat1ad8j|xn--mgbaam7a8h|xn--mgbayh7gpa|xn--mgbbh1a71e|xn--mgbc0a9azcg|xn--mgberp4a5d4ar|xn--o3cw4h|xn--ogbpf8fl|xn--p1ai|xn--pgbs0dh|xn--s9brj9c|xn--wgbh1c|xn--wgbl6a|xn--xkc2al3hye2a|xn--xkc2dl3a5ee0h|xn--yfro4i67o|xn--ygbi2ammx|xn--zckzah|xxx|ye|yt|za|zm|zw))(?![a-z]))|(?<=://)/))(?:(?:[^\\s()<>]+|\\((?:[^\\s()<>]+|(?:\\([^\\s()<>]*\\)))*\\))*)(?<![\\s`!()\\[\\]{};:'\".,<>?«»“”‘’])"
                              captureIndex:0];
    
    NSMutableArray *URLs = [[NSMutableArray alloc] initWithCapacity:links.count];
    for (NSString *link in links) {
      NSURL *URL = [NSURL HTTPURLFromString:link];
      [URLs addObject:URL];
    }
    return URLs;
  }
  return nil;
}

/**
 Get all URLs in the text using NSDataDetector. We have some troubles with: genk.vn, tinhte.vn, soha.vn (they are have '.vn' at domain)
 @param text The string need to be detect links
 @return list of NSURL in text
 */
- (NSArray *)linksByNSDataDetector:(NSString *)text
{
  if (text) {
    NSError *error = nil;
    NSDataDetector *detector= [[NSDataDetector alloc] initWithTypes:NSTextCheckingTypeLink
                                                              error:&error];
    
    NSMutableArray *matches = [[NSMutableArray alloc] init];
    
    [detector enumerateMatchesInString:text
                               options:0
                                 range:NSMakeRange(0, [text length])
                            usingBlock:^(NSTextCheckingResult *result, NSMatchingFlags flags, BOOL *stop) {
                              
                              if (result.resultType == NSTextCheckingTypeLink) {
                                [matches addObject:result.URL];
                              }
                            }];
    return matches;
  }
  return nil;
}

/**
 Get all emotions in the text, which are alphanumeric strings, no longer than 15 characters, contained in parenthesis
 @param text The string need to be detect links
 @return list of NSString emoticons in text
 */
- (NSArray *)emoticonsDetector:(NSString *)text
{
  // \\( : Start with '('
  // ([A-Za-z0-9]{1,15}) : Capture - Alphanumeric strings, no longer than 15 characters
  // \\) : End with ')'
  return [text detectByPattern:@"\\(([A-Za-z0-9]{1,15})\\)"
                  captureIndex:1];
}


/**
 Get all mentions in the text, they always starts with an '@' and ends when hitting a non-word character
 @return list of NSString mentions in text
 */
- (NSArray *)mentionsDetector:(NSString *)text
{
  /*
   CASE 1
   Pattern: (?:\\s+|^)@(\\w+)
   Explain:
   (?:\\s+|^) : Matches - Any string begin with space or empty or '(' or '{'
   @ : They have to start with an '@'
   (\\w+) : Capture the string must be words, end with a non-word
   */
  
  NSString *pattern = @"(?:\\s+|\\W|^)@(\\w+)";
  NSArray *raw = [text detectByPattern:pattern
                          captureIndex:1];
  NSMutableArray *mentions = [[NSMutableArray alloc] init];
  while (raw && raw.count > 0) {
    NSString *mention = [raw firstObject];
    [mentions addObject:mention];
    text = [text stringByReplacingOccurrencesOfString:[@"@" stringByAppendingString:mention] withString:@""];
    raw = [text detectByPattern:pattern
                   captureIndex:1];
  }
  return mentions;
  
  
  /*
   Input: man@man
   and pattern is: @(\\w+) 
   the matches will contain mention: 'man'
   This test case is wrong!
   */
//  return [text detectByPattern:@"@(\\w+) "
//                  captureIndex:1];
}

@end
