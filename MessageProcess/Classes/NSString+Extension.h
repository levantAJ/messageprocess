//
//  NSString+Extension.h
//  MessageProcess
//
//  Created by AJ on 6/4/15.
//  Copyright (c) 2015 AJ. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (Extension)

/**
 Method to check the string is empty
 */
- (BOOL)isNotEmpty;

/**
 Method to check the string is blank
 Blank string only contains space and new line characters
 */
- (BOOL)isNotBlank;

/**
 Get list of strings fit with pattern
 @param pattern The regex pattern which will identify the string
 @param captureIndex The index of capture in pattern: 0: all
 @return list of NSString captured with pattern
 */
- (NSArray *)detectByPattern:(NSString *)pattern
                captureIndex:(NSInteger)captureIndex;


@end
