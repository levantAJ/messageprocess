//
//  MessageCell.h
//  MessageProcess
//
//  Created by AJ on 6/4/15.
//  Copyright (c) 2015 AJ. All rights reserved.
//

#import <UIKit/UIKit.h>

@class Message;

@interface MessageCell : UITableViewCell

@property (nonatomic, weak) IBOutlet UILabel *contentLabel;
@property (nonatomic, weak) IBOutlet UIView *wrapperView;
@property (nonatomic, weak) IBOutlet UILabel *componentsLabel;

/**
 Get UINib when the tableView need registerNib
 @return UINib with identifier is reuseId
 */
+ (UINib *)nib;

/**
 Get identifier of cell
 @return String for when the uitableviewcell reuse
 */
+ (NSString *)reuseId;

/**
 @param width Maximum width of cell
 @return height off cell
 */
- (CGFloat)heightForWidth:(CGFloat)width;

/**
 Configurate the appearence of all control in cell
 @param messave The message will be displayed info on cell
 */
- (void)configureForMessage:(Message *)message;

@end
