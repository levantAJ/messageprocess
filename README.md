# Coding Task - Message Process #

* Summary: The task to detect mentions, emotion, and link with its title in a message
* Version: 1.0

# Installation #

* Dependencies: [HTMLReader](https://github.com/nolanw/HTMLReader), [Reachability](https://github.com/tonymillion/Reachability)
* How to run tests: ```command``` ```+``` ```u```

# How to use #
```objc
#import "Message.h"
```

### 1. Create a message:###
```objc
Message *message = [[Message alloc] initWithText:@"https://www.atlassian.com, @levantAJ, (pokerface)"];
```

### 2. Receive the components from text of message. You have two way to get components (links, mentions, emoticons) ###

#### 2.1 Via completion block ####
```objc
[message componentsWithCompletion:^(NSString *components) {
      NSLog(@"Components: %@", components);
    }];
```


#### 2.2 Via delegate ####
Add MessageDelegate first:
```objc
@interface MessageViewController () <UITableViewDelegate, UITextViewDelegate, MessageDelegate>
```

```
message.delegate = self;
[message getComponents];
```
And implement the delegate method:
```objc
- (void)message:(Message *)message
didGetComponents:(NSString *)components
{
  NSLog(@"Components: %@", components);
}
```

# Demo #
Build and run the example project in Xcode: ```"https://www.atlassian.com, @levantAJ, (pokerface)"```
![iOS Simulator Screen Shot Jun 8, 2015, 4.49.05 PM.png](https://bitbucket.org/repo/gMz65d/images/3177619254-iOS%20Simulator%20Screen%20Shot%20Jun%208,%202015,%204.49.05%20PM.png)

# Contact #
[Le Van Tai](https://github.com/levantai) [@levantAJ](https://twitter.com/levantAJ)